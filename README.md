# CMS ROOT Docker

ROOT containers for CMS Open Data.

## Build
```
docker build -t cms-root-docker .
```

## Run
```
docker run --rm -it cms-root-docker:latest
```

For example:
```
~/cms-root-docker$ docker run --rm -it cms-root-docker:latest
cmsusr@ed7c5313aded:~$ pwd
/home/cmsusr
cmsusr@ed7c5313aded:~$ root
   ------------------------------------------------------------------
  | Welcome to ROOT 6.26/00                        https://root.cern |
  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |
  | Built for linuxx8664gcc on Mar 03 2022, 06:51:13                 |
  | From tags/v6-26-00@v6-26-00                                      |
  | With c++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0                    |
  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |
   ------------------------------------------------------------------

root [0] .q
cmsusr@ed7c5313aded:~$ root-config --cflags --libs
-pthread -std=c++14 -m64 -I/opt/root/include -L/opt/root/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -lROOTDataFrame -Wl,-rpath,/opt/root/lib -pthread -lm -ldl -rdynamic
```